package Tests;

import Helper.TestListener;
import Pages.*;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.List;

public class AddToCartTest extends BaseTest{
    // D-M
    private double totalPrice;
    private boolean cartIsEmpty;
    private List<Integer> listOfQuantity;

    @BeforeClass
    public void loginForSystem() {
        super.Login();
        CartPage cartP = new CartPage(super.driver, super.wait);
        cartP.clickOnShoppingCartButton();
        this.totalPrice = cartP.getTotalPrice();
        this.cartIsEmpty = cartP.cartIsEmpty();
        if (!this.cartIsEmpty){
            this.listOfQuantity = cartP.getListOfQuantities();
        }

        cartP.clickOnReturnHomeButton();
    }

    @Test
    public void addToCartTest() {
        // Variable for assert
        String tempID;

        HomePage homeP = new HomePage(super.driver, super.wait);
        AnimalPage animalP = new AnimalPage(super.driver, super.wait);
        TypeOfAnimalPage typeOfAnimalP = new TypeOfAnimalPage(super.driver, super.wait);
        DetailsOfAnimalPage detailsOfAnimalP = new DetailsOfAnimalPage(this.driver, this.wait);
        CartPage cartP = new CartPage(super.driver, super.wait);
        homeP.clickOnRandomAnimal();
        TestListener.test.info("click on random animal");
        animalP.clickOnRandomTypeAnimal();
        typeOfAnimalP.clickOnRandomSubtypeAnimal();
        tempID = detailsOfAnimalP.getTempID();
        detailsOfAnimalP.clickOnAddToCart();

        if (cartIsEmpty) {
            Assert.assertEquals(tempID, cartP.getTempIDByIndex(0));
            Assert.assertTrue(cartP.getTotalPrice() > this.totalPrice);
        } else {
            if (this.listOfQuantity.size() < cartP.getListOfQuantities().size()){
                boolean isExists = false;
                for (int i = 0; i < cartP.getListOfQuantities().size() && !isExists; i++) {
                    isExists = tempID.equals(cartP.getTempIDByIndex(i));
                }

                Assert.assertTrue(isExists);
            } else {
                int index;
                boolean isExists = false;
                for (index = 0; index < this.listOfQuantity.size() && !isExists; index++){
                    isExists = tempID.equals(cartP.getTempIDByIndex(index));
                }

                Assert.assertTrue(isExists);
                Assert.assertEquals((int) cartP.getListOfQuantities().get(index),
                        (this.listOfQuantity.get(index) + 1));
            }
        }

        cartP.clickOnReturnHomeButton();
    }
    @Test
    public void addToCartTest2() {
        CartPage cartP = new CartPage(super.driver, super.wait);
        cartP.clickOnShoppingCartButton();
        try{
            Thread.sleep(3000);
        }catch (Exception exception) {

        }
        this.totalPrice = cartP.getTotalPrice();
        this.cartIsEmpty = cartP.cartIsEmpty();
        if (!this.cartIsEmpty){
            this.listOfQuantity = cartP.getListOfQuantities();
        }
        cartP.clickOnReturnHomeButton();



        // Variable for assert
        String tempID;

        HomePage homeP = new HomePage(super.driver, super.wait);
        AnimalPage animalP = new AnimalPage(super.driver, super.wait);
        TypeOfAnimalPage typeOfAnimalP = new TypeOfAnimalPage(super.driver, super.wait);
        DetailsOfAnimalPage detailsOfAnimalP = new DetailsOfAnimalPage(this.driver, this.wait);
        //CartPage cartP = new CartPage(super.driver, super.wait);
        homeP.clickOnRandomAnimal();
        TestListener.test.info("click on random animal");
        animalP.clickOnRandomTypeAnimal();
        try{
            Thread.sleep(3000);
        }catch (Exception exception) {

        }
        typeOfAnimalP.clickOnRandomSubtypeAnimal();

        tempID = detailsOfAnimalP.getTempID();
        detailsOfAnimalP.clickOnAddToCart();

        if (cartIsEmpty) {
            System.out.println("sdfghjkl1234567890");
            Assert.assertEquals(tempID, cartP.getTempIDByIndex(0));
            Assert.assertTrue(cartP.getTotalPrice() > this.totalPrice);
        } else {
            if (this.listOfQuantity.size() < cartP.getListOfQuantities().size()){
                boolean isExists = false;
                for (int i = 0; i < cartP.getListOfQuantities().size() && !isExists; i++) {
                    isExists = tempID.equals(cartP.getTempIDByIndex(i));
                }

                Assert.assertTrue(isExists);
            } else {
                int index;
                boolean isExists = false;
                for (index = 0; index < this.listOfQuantity.size() && !isExists; index++){
                    isExists = tempID.equals(cartP.getTempIDByIndex(index));
                }

                Assert.assertTrue(isExists);
                Assert.assertEquals((int) cartP.getListOfQuantities().get(index), (this.listOfQuantity.get(index) + 1));
            }
        }


        //Assert.assertTrue(false);
    }
}
