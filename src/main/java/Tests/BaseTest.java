package Tests;

import Pages.LandingPage;
import Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class
BaseTest {
    // final members
    final String USER_NAME = "shmuel";
    final String PASSWORD = "123456";
    final String PATH = "http://localhost:8080/jpetstore";

    // Variable definition
    protected WebDriver driver;
    protected WebDriverWait wait;

    @BeforeTest
    public void setUP() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\jars for selenium\\chromedriver_win32\\chromedriver.exe");

        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.get(PATH);
        LandingPage landingScreenP = new LandingPage(this.driver, this.wait);
        landingScreenP.clickOnEnterToStore();
    }

    @AfterTest
    public void terminate() {
        driver.quit();
    }

    public void Login() {
        LoginPage loginP = new LoginPage(this.driver, this.wait);
        loginP.clickOnSignInButton();
        loginP.sendKeysUserNameField(this.USER_NAME);
        loginP.sendKeysPasswordField(this.PASSWORD);
        loginP.clickOnLoginButton();
    }
}
