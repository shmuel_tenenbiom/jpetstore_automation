package Tests;

import Pages.RegisterPage;
import org.testng.annotations.Test;

public class RegisterTest extends BaseTest{
    // final members
    final String USER_NAME = "DOGS";
    final String PASSWORD = "123456";
    final String FIRST_NAME = "shmuel";
    final String LAST_NAME = "tenenboim";
    final String EMAIL = "qwertyu@gmail.com";
    final String PHONE = "1234567890";
    final String ADDRESS1 = "qwerty";
    final String ADDRESS2 = "werty";
    final String CITY = "tel aviv";
    final String STATE = "north";
    final String ZIP = "12345676543";
    final String COUNTRY = "israel";
    final String LANGUAGE_PREFERENCE = "japanese";
    final String FAVOURITE_CATEGORY = "DOGS";

    @Test
    public void registerTest() {
        register();
    }

    // method register
    public void register() {
        RegisterPage registerP = new RegisterPage(super.driver, super.wait);
        registerP.clickOnSignInButton();
        registerP.clickOnRegisterButton();
        registerP.sendKeysUserName(this.USER_NAME);
        registerP.sendKeysPassword(this.PASSWORD);
        registerP.sendKeysRepeatPassword(this.PASSWORD);
        registerP.sendKeysFirstName(this.FIRST_NAME);
        registerP.sendKeysLastName(this.LAST_NAME);
        registerP.sendKeysEmail(this.EMAIL);
        registerP.sendKeysPhone(this.PHONE);
        registerP.sendKeysAddress1(this.ADDRESS1);
        registerP.sendKeysAddress2(this.ADDRESS2);
        registerP.sendKeysCity(this.CITY);
        registerP.sendKeysState(this.STATE);
        registerP.sendKeysZip(this.ZIP);
        registerP.sendKeysCountry(this.COUNTRY);
        registerP.selectLanguagePreference(this.LANGUAGE_PREFERENCE);
        registerP.selectFavouriteCategory(this.FAVOURITE_CATEGORY);
        registerP.clickOnNewAccountButton();
    }
}
