package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DetailsOfAnimalPage extends ConstantPage {
    // D-M
    @FindBy(css = "a.Button")
    private WebElement addToCartButton;

    @FindBy(tagName = "b")
    private WebElement tempID;

    // Constructor
    public DetailsOfAnimalPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public void clickOnAddToCart() {
        super.waitAndClick(this.addToCartButton);
    }

    public String getTempID() {
        return this.tempID.getAttribute("innerText");
    }
}
