package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.Random;

public class TypeOfAnimalPage extends ConstantPage {
    // D-M
    @FindBy(xpath = "//td[1]/a")
    private List<WebElement> listOfTempID;

    private Random rand = new Random();

    // Constructor
    public TypeOfAnimalPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public void clickOnRandomSubtypeAnimal() {
        super.waitAndClick(this.listOfTempID.get(rand.nextInt(this.listOfTempID.size())));
    }
}
