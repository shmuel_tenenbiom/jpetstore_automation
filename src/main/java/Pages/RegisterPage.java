package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage extends ConstantPage {
    // D-M
    @FindBy(name = "username")
    private WebElement userNameField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(name = "repeatedPassword")
    private WebElement repeatPasswordFiled;

    @FindBy(name = "account.firstName")
    private WebElement firstNameFiled;

    @FindBy(name = "account.lastName")
    private WebElement lastNameFiled;

    @FindBy(name = "account.email")
    private WebElement emailFiled;

    @FindBy(name = "account.phone")
    private WebElement phoneFiled;

    @FindBy(name = "account.address1")
    private WebElement address1Filed;

    @FindBy(name = "account.address2")
    private WebElement address2Filed;

    @FindBy(name = "account.city")
    private WebElement cityFiled;

    @FindBy(name = "account.state")
    private WebElement stateFiled;

    @FindBy(name = "account.zip")
    private WebElement zipFiled;

    @FindBy(name = "account.country")
    private WebElement countryFiled;

    @FindBy(name = "account.languagePreference")
    private WebElement languagePreferenceSelect;

    @FindBy(name = "account.favouriteCategoryId")
    private WebElement favouriteCategorySelect;

    @FindBy(name = "newAccount")
    private WebElement newAccountButton;

    @FindBy(css = "[href = '/jpetstore/actions/Account.action?newAccountForm=']")
    private WebElement registerButton;

    private By byForLanguagePreferenceSelectOptions =
            By.cssSelector("[name = 'account.languagePreference'] option");
    private By byForFavouriteCategorySelect =
            By.cssSelector("[name = 'account.favouriteCategoryId'] option");

    // Constructor
    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods send keys
    public void sendKeysUserName(String userName) {
        super.waitAndClearAndSendKeys(this.userNameField, userName);
    }

    public void sendKeysPassword(String password) {
        super.waitAndClearAndSendKeys(this.passwordField, password);
    }

    public void sendKeysRepeatPassword(String repeatPassword) {
        super.waitAndClearAndSendKeys(this.repeatPasswordFiled, repeatPassword);
    }

    public void sendKeysFirstName(String firstName) {
        super.waitAndClearAndSendKeys(this.firstNameFiled, firstName);
    }

    public void sendKeysLastName(String lastName) {
        super.waitAndClearAndSendKeys(this.lastNameFiled, lastName);
    }

    public void sendKeysEmail(String email) {
        super.waitAndClearAndSendKeys(this.emailFiled, email);
    }

    public void sendKeysPhone(String phone) {
        super.waitAndClearAndSendKeys(this.phoneFiled, phone);
    }

    public void sendKeysAddress1(String address1) {
        super.waitAndClearAndSendKeys(this.address1Filed, address1);
    }

    public void sendKeysAddress2(String address2) {
        super.waitAndClearAndSendKeys(this.address2Filed, address2);
    }

    public void sendKeysCity(String city) {
        super.waitAndClearAndSendKeys(this.cityFiled, city);
    }

    public void sendKeysState(String state) {
        super.waitAndClearAndSendKeys(this.stateFiled, state);
    }

    public void sendKeysZip(String zip) {
        super.waitAndClearAndSendKeys(this.zipFiled, zip);
    }

    public void sendKeysCountry(String country) {
        super.waitAndClearAndSendKeys(this.countryFiled, country);
    }

    // Methods selection
    public void selectLanguagePreference(String languagePreference) {
        super.waitAndSelect(this.languagePreferenceSelect,
                this.byForLanguagePreferenceSelectOptions, languagePreference);
    }

    public void selectFavouriteCategory(String favouriteCategoryId) {
        super.waitAndSelect(this.favouriteCategorySelect,
                this.byForFavouriteCategorySelect, favouriteCategoryId);
    }

    // Methods click
    public void clickOnNewAccountButton() {
        super.waitAndClick(this.newAccountButton);
    }

    public void clickOnRegisterButton() {
        super.waitAndClick(this.registerButton);
    }
}
