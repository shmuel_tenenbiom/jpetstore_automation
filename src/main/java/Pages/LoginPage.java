package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends ConstantPage {
    // D-M
    @FindBy(name = "username")
    private WebElement userNameField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(name = "signon")
    private WebElement loginButton;

    // Constructor
    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public void sendKeysUserNameField(String userName) {
        super.waitAndClearAndSendKeys(this.userNameField, userName);
    }

    public void sendKeysPasswordField(String password) {
        super.waitAndClearAndSendKeys(this.passwordField, password);
    }

    public void clickOnLoginButton() {
        super.waitAndClick(this.loginButton);
    }
}
