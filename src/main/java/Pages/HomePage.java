package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.Random;

public class HomePage extends ConstantPage {
    // D-M
    @FindBy(css = "map area")
    private List<WebElement> listOfAnimals;

    private Random rand = new Random();

    // Constructor
    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public void clickOnRandomAnimal() {
        super.waitAndClick(this.listOfAnimals.get(rand.nextInt(this.listOfAnimals.size())));
    }
}
