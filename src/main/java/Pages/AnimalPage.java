package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class AnimalPage extends ConstantPage {
    // D-M
    @FindBy(css = "table a")
    private List<WebElement> typeOfAnimal;

    private Random rand = new Random();


    // Constructor
    public AnimalPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public void clickOnRandomTypeAnimal() {
        this.typeOfAnimal.get(rand.nextInt(this.typeOfAnimal.size())).click();
    }
}
