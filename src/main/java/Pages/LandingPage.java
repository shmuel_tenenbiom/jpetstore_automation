package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LandingPage extends PageObject {
    // D-M
    @FindBy(tagName = "a")
    private WebElement enterToStore;

    // Constructor
    public LandingPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Method click
    public void clickOnEnterToStore() {
        super.waitAndClick(this.enterToStore);
    }
}
