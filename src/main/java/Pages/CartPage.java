package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.List;

public class CartPage extends ConstantPage {
    // D-M
    @FindBy(tagName = "td")
    private List<WebElement> listForTotalPrice;

    @FindBy(css = "[colspan = '8']")
    private WebElement cartIsEmpty;

    @FindBy(xpath = "//tr/td[5]/input")
    private List<WebElement> listOfQuantity;

    @FindBy(linkText = "Return to Main Menu")
    private WebElement returnHomeButton;

    @FindBy(xpath = "//tr/td[1]")
    private List<WebElement> listOfTempID;

    // Constructor
    public CartPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public double getTotalPrice() {
        return Double.parseDouble(this.listForTotalPrice.
                get(this.listForTotalPrice.size() - 2).getAttribute("innerText")
                .split("\\$")[1]);
    }

    public void clickOnReturnHomeButton() {
        super.waitAndClick(this.returnHomeButton);
    }

    public boolean cartIsEmpty() {
        try{
            wait.until(ExpectedConditions.visibilityOf(this.cartIsEmpty));
            return true;
        } catch (Exception exception){
            return false;
        }
    }

    public String getTempIDByIndex(int index) {
        return this.listOfTempID.get(index).getAttribute("innerText");
    }
    
    public List<Integer> getListOfQuantities() {
        List<Integer> result = new ArrayList<Integer>();
        for (WebElement quantity: this.listOfQuantity) {
            result.add(Integer.parseInt(quantity.getAttribute("defaultValue")));
        }
        return result;
    }
}
