package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConstantPage extends PageObject {
    // D-M
    @FindBy(css = "[href = '/jpetstore/actions/Catalog.action']")
    protected WebElement homeButton;

    @FindBy(css = "[href = '/jpetstore/actions/Cart.action?viewCart=']")
    protected WebElement shoppingCartButton;

    @FindBy(linkText = "Sign In")
    protected WebElement signInButton;

    @FindBy(css = "[href = '../help.html']")
    protected WebElement helpButton;

    @FindBy(name = "keyword")
    protected WebElement searchField;

    @FindBy(name = "searchProducts")
    protected WebElement searchButton;

    // Constructor
    public ConstantPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    // Methods
    public void clickOnHomeButton() {
        super.waitAndClick(this.homeButton);
    }

    public void clickOnShoppingCartButton() {
        super.waitAndClick(this.shoppingCartButton);
    }

    public void clickOnSignInButton() {
        super.waitAndClick(this.signInButton);
    }

    public void clickOnHelpButton() {
        super.waitAndClick(this.helpButton);
    }

    public void clickOnSearchButton() {
        super.waitAndClick(this.searchButton);
    }

    public void sendKeysSearchField(String value) {
        super.waitAndClearAndSendKeys(this.searchField, value);
    }
}
