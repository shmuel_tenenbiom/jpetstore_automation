package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject {
    // D-M
    protected WebDriver driver;
    protected WebDriverWait wait;

    // Constructor
    public PageObject(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(this.driver, this);
    }

    // Methods
    public void waitAndClick(WebElement button) {
        this.wait.until(ExpectedConditions.elementToBeClickable(button)).click();
    }

    public void waitAndClearAndSendKeys(WebElement field, String value) {
        this.wait.until(ExpectedConditions.visibilityOf(field)).clear();
        field.sendKeys(value);
    }

    public void waitAndSelect(WebElement element, By byForList, String valueForSelect) {
         wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(byForList, 0));
        Select select = new Select(element);
        select.selectByValue(valueForSelect);
    }
}
